#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:67108864:6da2f9b50d4fbfb1c711690a178e04176ebc44fa; then
  applypatch  EMMC:/dev/block/bootdevice/by-name/boot:134217728:7fce55355c973074215f75fda610c183b5502ce6 EMMC:/dev/block/bootdevice/by-name/recovery 6da2f9b50d4fbfb1c711690a178e04176ebc44fa 67108864 7fce55355c973074215f75fda610c183b5502ce6:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
